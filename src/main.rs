use failure::{Error, ResultExt};
use git2::{FetchOptions, RemoteCallbacks};
use git2_credentials::CredentialHandler;
use rayon::prelude::*;
use serde_derive::Deserialize;
use std::fs;

#[derive(Deserialize)]
#[serde(tag = "type")]
enum ServerConfig {
    Gitlab { url: String, token: String },
}

#[derive(Deserialize)]
struct Config {
    base_path: String,
    servers: Vec<ServerConfig>,
}

fn get_fetch_options() -> Result<FetchOptions<'static>, git2::Error> {
    let config = git2::Config::open_default()?;
    let mut callbacks = RemoteCallbacks::new();
    let mut opts = FetchOptions::new();
    let mut handler = CredentialHandler::new(config);
    callbacks.credentials(move |url, username, allowed| {
        handler.try_next_credential(url, username, allowed)
    });
    opts.remote_callbacks(callbacks);
    opts.download_tags(git2::AutotagOption::All);
    opts.update_fetchhead(true);
    Ok(opts)
}

fn clone_repo(repo_url: &str, path: &str) -> Result<git2::Repository, git2::Error> {
    let mut repo = git2::build::RepoBuilder::new();
    let checkout = git2::build::CheckoutBuilder::new();
    let fetch = get_fetch_options()?;
    repo.fetch_options(fetch)
        .with_checkout(checkout)
        .clone(repo_url, std::path::Path::new(path))
}

fn fetch_repo(repo: git2::Repository) -> Result<(), git2::Error> {
    let mut fetch = get_fetch_options()?;
    repo.find_remote("origin")?
        .fetch(&[], Some(&mut fetch), None)
}

fn sync_gitlab_project(project: &gitlab::Project, base_path: &str, url: &str) -> Result<(), Error> {
    let repo_url = &project.http_url_to_repo;
    let path = format!(
        "{}/{}/{}/{}",
        base_path, url, project.namespace.full_path, project.path
    );
    fs::create_dir_all(&path)?;
    let repo = match git2::Repository::open(&path) {
        Ok(repo) => repo,
        Err(_) => {
            clone_repo(repo_url, &path).context(format!("Could not clone repo {}", repo_url))?
        }
    };
    fetch_repo(repo).context(format!("Could not fetch from remote {}", repo_url))?;
    Ok(())
}

fn sync_gitlab(base_path: &str, url: &str, token: &str) -> Result<(), Error> {
    let client = gitlab::Gitlab::new(url, token).map_err(failure::SyncFailure::new)?;
    client
        .projects(&[("membership", "true")])
        .map_err(failure::SyncFailure::new)?
        .par_iter()
        .map(|project| sync_gitlab_project(project, base_path, url))
        .collect()
}

fn get_config() -> Result<Config, Error> {
    let xdg = xdg::BaseDirectories::with_prefix("reposync")?;
    let dir = xdg.place_config_file("reposync.toml")?;
    let content = fs::read_to_string(dir)?;
    toml::from_str(&content).map_err(Into::into)
}

fn sync_server(base_path: &str, config: ServerConfig) -> Result<(), Error> {
    match config {
        ServerConfig::Gitlab { url, token } => sync_gitlab(base_path, &url, &token)?,
    }
    Ok(())
}

fn main() -> Result<(), Error> {
    let config = get_config()?;
    for server in config.servers {
        sync_server(&config.base_path, server)?;
    }
    Ok(())
}
